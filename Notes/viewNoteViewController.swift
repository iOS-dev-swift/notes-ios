//
//  viewNoteViewController.swift
//  Notes
//
//  Created by Cohen, Dor on 22/09/2020.
//

import UIKit

class viewNoteViewController: UIViewController {

    var previuosVC = NotesTableViewController()
    var note : Notes? = nil
    var noteId : Int? = nil
    
    @IBOutlet weak var noteName: UITextField!
    @IBOutlet weak var noteDescription: UITextField!
    @IBOutlet weak var noteImportant: UISwitch!
    
    
    @IBAction func editNote(_ sender: Any) {
        previuosVC.notes[noteId!].name = noteName.text!
        previuosVC.notes[noteId!].description = noteDescription.text!
        previuosVC.notes[noteId!].important = noteImportant.isOn
        previuosVC.tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func noteComplete(_ sender: Any) {
        previuosVC.notes.remove(at: noteId!)
        previuosVC.tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noteName.text = note!.name
        noteDescription.text = note!.description
        noteImportant.setOn(note!.important, animated: true)
    }

}
