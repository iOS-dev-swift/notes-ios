//
//  NotesClass.swift
//  Notes
//
//  Created by Cohen, Dor on 21/09/2020.
//

import UIKit

class Notes{
    var name : String  = ""
    var description : String = ""
    var important : Bool = false
    var creationDate : Date = Date()
}
