//
//  NotesTableViewController.swift
//  Notes
//
//  Created by Cohen, Dor on 21/09/2020.
//

import UIKit

class NotesTableViewController: UITableViewController {

    var notes : [Notes] = []
    
    func initializeData() -> [Notes]{
        let note1 = Notes()
        note1.name = "Buy Eggs"
        note1.description = "Need to go to the store and buy some eggs"
        note1.important = true
        
        let note2 = Notes()
        note2.name = "Buy Milk"
        note2.description = "Need to go to the store and buy some milk"
        
        let note3 = Notes()
        note3.name = "Call Wife"
        note3.description = "Need to call my wife"
        
        return [note1, note2, note3]
    }
    
    override func viewDidLoad() {
        notes = initializeData()
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notesCell", for: indexPath)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm E, d MMM y"
        
        cell.textLabel?.text = "\(notes[indexPath.row].important ? "❗️" : "") \(notes[indexPath.row].name) - \(dateFormatter.string(from : notes[indexPath.row].creationDate))"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "moveToViewNote", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addVC = segue.destination as? CreateNoteViewController{
            addVC.previuosVC = self
        }
        if let viewVC = segue.destination as? viewNoteViewController{
            if let noteId = sender as? Int{
                viewVC.note = notes[noteId]
                viewVC.noteId = noteId
                viewVC.previuosVC = self
            }
        }
    }
}
