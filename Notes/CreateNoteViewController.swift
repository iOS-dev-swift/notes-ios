//
//  CreateNoteViewController.swift
//  Notes
//
//  Created by Cohen, Dor on 21/09/2020.
//

import UIKit

class CreateNoteViewController: UIViewController {
    
    var previuosVC = NotesTableViewController()

    @IBOutlet weak var noteName: UITextField!
    @IBOutlet weak var noteDescription: UITextField!
    @IBOutlet weak var noteImportant: UISwitch!
    
    @IBAction func createNote(_ sender: Any) {
        let newNote = Notes()
        if noteName.text != nil && noteDescription.text != nil{
            newNote.name = noteName.text!
            newNote.description = noteDescription.text!
            newNote.important = noteImportant.isOn
            
            previuosVC.notes.append(newNote)
            previuosVC.tableView.reloadData()
            navigationController?.popViewController(animated: true)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
